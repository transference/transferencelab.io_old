# Transference

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## About

Transference aims to bring real-time location and arrival time estimation using deep learning.

## Build Status

Transference Site: ![Build Status](https://gitlab.com/transference/transference.gitlab.io/badges/master/build.svg)
Transference Instance Logulator: ![Build Status](https://gitlab.com/transference/instance/logulator/badges/master/build.svg)
Transference Instance Logulator (packaging): ![Build Status](https://gitlab.com/transference/instance/logulator/badges/packaging/build.svg)

## About this Site

Generated using GitBook and GitLab CI. Forked from [GitLab Pages](https://gitlab.com/pages/gitbook). Content by [@colourdelete](https://gitlab.com/colourdelete)